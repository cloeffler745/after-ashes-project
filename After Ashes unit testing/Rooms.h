#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Objects.h"
#include "Beings.h"



class List;
class Iterator;

class Node { //this will eventually become a custom list, so Room is called Node
public: 
	Node() = default; 
	Node(std::string d) { desc = d; };
	void litter(Item& junk); //give room item
	void rape(Item& key); //take item from room
	bool checkR(std::string want);//checks Room for item
	void look(); //gives room description
private: 
	std::vector<Item*> Storage = {};
	std::string desc = "";
	Node* previousN = NULL;
	Node* nextN = NULL;
	friend class Iterator;
	friend class List;
};


/* ****************************************************************************************************

OTHER LIST CLASSES, FOR MOVEMENT AND SUCH, DO NOT EDIT FURTHER

******************************************************************************************************** */

class List {
public:
	List();
	void push_back(std::string filename);
	Iterator erase(Iterator iter);
	Iterator begin();
	Iterator end();
private:
	Node* firstRoom = NULL;
	Node* lastRoom = NULL;
	friend class Iterator;
};

class Iterator {
public:
	Iterator();
	Node& get();//return pointer to Node
	void next();
	void previous();
	bool equals(Iterator b) const;
private:
	Node* position = NULL;
	List* container = NULL;
	friend class List;
};

