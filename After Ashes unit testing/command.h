#pragma once
#include<string>
#include<iostream>
#include"Objects.h"
#include"Beings.h"
#include"Rooms.h"
//Set of functions that deal with user input

//delete trailing spaces, remove punctuation
//make all letters lower case

void COMMAND(std::string& barf, std::vector<Item>& W, player& user); // main command code

bool nope(std::string& barf); //false if the string is over 25 characters long, nothing the computer can take is longer

void seperate(std::string& barf, std::string& verb, std::string& noun); 

bool fix(std::string& barf); //remove punctuation, delete trailing spaces, make all letters lower case
//^ essentially makes user input easier for program to read

Item& convert(std::string n, std::vector<Item>& W); //converts a string to an Item*

void doStuff(std::string& verb, std::string& noun, std::vector<Item>& W, player& user); //does what the user is asking 