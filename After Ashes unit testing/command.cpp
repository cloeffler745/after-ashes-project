#include "command.h"
#include<sstream>
#include<vector>
//#define NDEBUG
#include <cassert>

bool nope(std::string& barf) {
	if (barf.length() > 25)
		return false;
	return true; 
}

bool fix(std::string& barf) {
	std::vector<size_t> rid = {}; //things to delete from string
	int words = 1; //count the number of spaces (ie words)
	for (size_t i = 0; i < barf.length(); i++) {
		if (barf[i] < 91 && barf[i] > 64) {//if uppercase, make lowercase
			barf[i] = barf[i] + 32; 
		}
		else if (32 < barf[i] && barf[i] < 65) {//if punctuation keep track of i to remove later
			rid.push_back(i); 
		}
		if (islower(barf[i]) && i > 1 && barf[i - 1] == 32)//count the words
			words++;
	}
	if (rid.size() > 0) {//if there is something to remove
		for (int i = rid.size() - 1; i > -1; i--) {//delete unnessecary from end of string
			barf.erase(rid[i]);
		}
	}
	if (words < 3)//pass the fix test
		return true; 
	return false; //does not pass fix test
}

void seperate(std::string& barf, std::string& verb, std::string& noun) {//will put words into two strings
	std::istringstream get(barf);
	get >> verb >> noun;
	/* NEEDS WORK, FIX NEEDS TO DO WHAT I ALREADY SAY IT DOES AND IT ALSO HAS TO MAKE THE TEXT ONLY TWO WORDS
	FOR NOW "take the sword" makes the noun = the and "sword" is lost */
}

Item& convert(std::string n, std::vector<Item>& W) {//wll break if Item not in the list. Be carefull***************
#ifndef NDEBUG 
	bool present = false;
	for (size_t i = 0; i < W.size(); i++) {
		if (n == W[i].getName())
			present = true;
	}
	assert(present && "There is no such item.");
#endif
	for (size_t i = 0; i < W.size(); i++) {
		if (n == W[i].getName())
			return W[i];//return the Item if there
	}
}

void doStuff(std::string& verb, std::string& noun, std::vector<Item>& W, player& user) {//SHOULD PROBABLY IMPLIMENT SOME CATCH-LIKE THING
	if (verb == "look") {
		if (noun != "")
			std::cout << "The " << noun << " is an object you can pick up.\n";
		else
			user.place()->look();
	}

	else if (noun == "") //there needs to be a noun in the codes below
		std::cout << "ERROR: I think that sentence is missing a noun.\n";//ERROR MESSAGE

	else if (verb == "take") {
		Item& want = convert(noun, W);
		if (!user.checkLoad()) {//stops if you do not have enough room in inventory
			std::cout << "You do not have enough room in your inventory.\n"; 
			return;
		}
		user.get(want); //moves item to player inventory
		std::cout << "You have taken the " << noun << std::endl;
	}

	else if (verb == "drop") {
		if (!user.checkBag(noun)) {//stops command if the item is not in the users bag
			std::cout << "You do not have a " << noun << " in your bag.\n"; 
			return; 
		}
		Item& want = convert(noun, W);
		user.drop(want);
		std::cout << "You have dropped the " << noun << std::endl;
	}
	else if (verb == "search")
		user.view(); 
	else
		std::cout << "ERROR:I do not understand '" << verb << "'\n"; //ERROR MESSAGE
}

void COMMAND(std::string& barf, std::vector<Item>& W, player& user) {
	if (!nope(barf)) {
		std::cout << "ERROR: Your input is too long." << std::endl;//ERROR MESSAGE
		barf = "look"; // reset the command string to something usable by the rest of the function, in case does not end
		return;//should end the program
	}

	if (!fix(barf)) {
		std::cout << "ERROR: You may only use one or two word commands" << std::endl;//ERROR MESSAGE 
		barf = "look"; 
		return; //should end the program 
	}

	std::string v;
	std::string n; 

	seperate(barf, v, n);

	//ONLY IF THE TEXT IS GOOD AND READY SHOULD IT CONTINUE!!!!!!!!!
	doStuff(v, n, W, user); 
	barf = "";
}