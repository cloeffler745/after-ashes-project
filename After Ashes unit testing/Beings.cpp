#include <cassert>
//#define NDEBUG
#include"Beings.h"



//HELPER FUNCTIONS FOR PLAYER

bool player::checkLoad() {
	if (inventory.size() == capacity)
		return false; 
	return true; 
}

bool player::checkBag(std::string desire) {
	for (size_t i = 0; i < inventory.size(); i++) {
		if (desire == inventory[i]->getName())
			return true; 
	}
	std::cout << "You do not have any such item in your bag.\n";
	return false;
}

void player::get(Item& key) {
#ifndef NDEBUG
	assert(this->checkLoad() && "No space in bag");
#endif
		Item* thing = &key; 
		inventory.push_back(thing); //create pointer to the item and put it in the player inventory
}

void player::drop(Item& junk) {
#ifndef NDEBUG
	assert(this->checkBag(junk.getName()));
#endif
	std::vector<Item*>::iterator i = inventory.begin();
	while (i != inventory.end()) {
		if ((*i)->getName() == junk.getName())
			break;
		i++; 
	}
	inventory.erase(i); 
}

void player::view() {
	for (size_t i = 0; i < inventory.size(); i++) {
		std::cout << inventory[i]->getName() << ": " << inventory[i]->getDesc() << std::endl; 
	}
}

void player::exist(Node& loc) {//sets player location 
	location = &loc; 
}

Node* player::place() {
	return location->get(); 
}

void Node::look() {
	std::cout << desc << std::endl;
	for (size_t i = 0; i < Storage.size(); i++) {
		std::cout << "There is a " << (*Storage[i]).getName() << " lying on the ground." << std::endl; 
	}
	return; 
}