#pragma once
#include<string>

class Item {
public: 
	Item() = default; //WHAT DOES THIS DO? 
	Item(std::string n, std::string d) { name = n; desc = d; };
	std::string getName(); 
	std::string getDesc(); 
private: 
	std::string name = ""; 
	std::string desc = ""; 
};


//weapons needs to be it's own thing, has stats and is used in a different way