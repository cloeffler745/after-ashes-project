#pragma once
#include<string>
#include<vector>
#include<iostream>
#include"Objects.h"
#include"Rooms.h"


class player {
public:
	player() = default; //WHAT DOES THIS MEAN??????
	void get(Item& key); //puts an ITEM in the players inventory
	void drop(Item& junk); //removes an item from the players inventory
	void view(); // lists everything in the inventory
	bool checkLoad(); //checks that there is enough room in inventory for new ITEM
	bool checkBag(std::string desire); //checks bag for a specific item
	Node* place(); //returns the position of the player
	void exist(Node& loc); //sets player position 
private:
	Iterator location = NULL; //SHOULD BE IN PRIVATE FIX FIX FIX FIX FIX
	std::vector<Item*> inventory; 
	int capacity = 5; // the number of items you can hold
 };

//other characters and monsters to fight against and with