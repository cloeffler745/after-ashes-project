#include "Rooms.h"
#include "Objects.h"
#include "Beings.h"
#include <cassert>


/* ****************************************************************************************************

NODE

******************************************************************************************************** */


void Node::litter(Item& junk) {
	Storage.push_back(&junk); 
}

bool Node::checkR(std::string want) {
	for (size_t i = 0; i < Storage.size(); i++) {
		if (want == (*Storage[i]).getName())
			return true; 
	}
	return false; 
}

void Node::rape(Item& key) {
#ifndef NDEBUG
	assert(!checkR(key.getName()) && "Item not in room");
#endif
	std::vector<Item*>::iterator i = Storage.begin();
	while (i != Storage.end()) {
		if ((*i)->getName() == key.getName())
			break;
		i++;
	}
	Storage.erase(i);
}




/* ****************************************************************************************************

OTHER LIST CLASSES, FOR MOVEMENT AND SUCH, DO NOT EDIT FURTHER

******************************************************************************************************** */


/* ****************************************************************************************************

LIST

******************************************************************************************************** */

List::List() {
	firstRoom = NULL;
	lastRoom = NULL;
}

void List::push_back(std::string filename) {
	Node* new_node = new Node(filename);
	if (lastRoom == NULL) {
		firstRoom = new_node;
		lastRoom = new_node;
	}
	else {
		new_node->previousN = lastRoom;
		lastRoom->nextN = new_node;
		lastRoom = new_node;
	}
}


/* ****************************************************************************************************

ITERATOR

******************************************************************************************************** */

Iterator List::erase(Iterator iter) {
	assert(iter.position != NULL);
	Node* remove = iter.position;
	Node* before = remove->previousN;
	Node* after = remove->nextN;
	if (remove == firstRoom)
		firstRoom = after;
	else
		before->nextN = after;
	if (remove == lastRoom)
		lastRoom = before;
	else
		after->previousN = before;
	delete remove;
	Iterator r;
	r.position = after;
	r.container = this;
	return r;
}

Iterator List::begin() {
	Iterator iter;
	iter.position = firstRoom;
	iter.container = this;
	return iter;
}

Iterator List::end() {
	Iterator iter;
	iter.position = NULL;
	iter.container = NULL;
	return iter;
}

Iterator::Iterator() {
	position = NULL;
	container = NULL;
}

Node& Iterator::get(){
	assert(position != NULL);
	return *position;
}

void Iterator::next() {
	assert(position != NULL);
	position = position->nextN;
}

void Iterator::previous() {
	assert(position != container->firstRoom);
	if (position == NULL)
		position = container->lastRoom;
	else
		position = position->previousN;
}

bool Iterator::equals(Iterator b) const {
	return position == b.position;
}

