**ROOMS, not master, is the working branch.**

This project is a texted based adventure game. You can fight monsters, but the monsters do not fight back. 
There are items(weapons and potions, both derived classes of Items) that you can pick up and drop. You can travel 
between locations and  check your inventory. 

What I have learned/topics used from class thus far:

- forced push onto remote repository using git will delete the readme file on bitbucket

- for_each is implemented along with lambda functions 

- Abstract class objects can ONLY be used to create pointers to derived class object

- There is an issue when you have two header files that call each other, but in my code I could not figure out what
it was I tried various methods to fix the issue between the Rooms and Beings headers (forward declarations, changing 
the #pragma to #ifndef, etc.) I could not fix the issue. I ended up talking to the professor, but he was not able to 
see the issue in the short time he had to glance at the code. Thus I had to move the class I needed in beings.h to 
Rooms.h. Although this temporarily fixed the problem, player(the remaining class in Beings.h) and Monster (the 
class I had to move to Rooms.h) share many functions and should  be derived from the same base class, I can no longer 
implement it this way however. 

- I fixed an issue with function pointers in the class, I have learned that you cannot have template AND define function 
pointer at the same time. I have also learned that given the following code: 

```
typedef void(*puzzle)(); 

void def() { return; };

class Node { 
	void def1() { return; }
	puzzle dungeon;
};
```

dungeon can be def(), but NOT def1(). So the puzzle function type cannot point to member functions of a class. 


- Smart pointers are great for the items of the world. I can have the room Storage vectors point to many of the 
items, and not have to worry about releasing the memory at the end or too early.

- For git, can put the readme and commit  on master and can still push w/o forcing to other branches of the remote 
and it will not cause an error. 

- Created a template function combat() that takes in two values by reference (meant to be either monster or player 
types), but the order that you put the parameters into the function determines which individual takes damage and 
which individual does the damage. 

- I am having an issue with the Combat function previously mentioned. The code for it are as follows: 

```
template<typename murderer, typename victom>
void Combat(murderer& m, victom& v) {
	int h = m.fWhat()->hip();
	m.attack();
	v.changeH(h);
}
```

For some reason when I put Combat (monster, player) the program fails to compile and it says that victom = monster. 
But this should be a template function! I can call this function using (player, monster) as parameters first and it 
will work, but then the program remembers which template was monster and which was player for the next function call
and it stops the compile. I am not sure how to fix this issue. So I have commented out the code that is broken.  